### Entitats i persones participants

* Impulsores

   Els impulsors d'aquest projecte ens reunim periòdicament per debatre les aportacion fetes, parlar de les seves mancances, així com  promoure la seva projecció pública. Per demanar més informació sobre aquestes reunions pots enviar un correu a [info@apoderamentdigital.cat](mailto:info@apoderamentdigital.cat).

    <div id="entitats" markdown="1">

    | | | |
    |:-------------------------:|:-------------------------:|:-------------------------:|
    | [![Sobtec](/imatges/sobtec.jpg) *Sobtec*](http://sobtec.cat) | [![Barcelona Free Software](/imatges/bcnfs.svg) *Barcelona Free Software*](https://bcnfs.org) | [![Pangea](/imatges/pangea.png) *Pangea*](https://pangea.org/)
    | [![Colectic](/imatges/colectic.png) *Colectic*](https://colectic.coop) | [![Pirates de Catalunya](/imatges/pirata.svg) *Pirates de Catalunya*](https://pirata.cat) | [![Xarxa d'Innovació Pública](/imatges/xip.png) *Xarxa d'Innovació Pública*](http://www.xarxaip.cat/)
    || [![guifi.net/Exo](/imatges/guifi.svg) *guifi.net/Exo*](https://guifi.net/) |

    </div>

* Contribuidores

   Els contribuidors s'encarreguen de fer propostes específiques per ampliar els diferents apartats de la guia.
   Trobaràs més informació sobre com contribuir al document [COM_CONTRIBUIR.md](/com_contribuir).

    <div id="entitats" markdown="1">

    | | | |
    |:-------------------------:|:-------------------------:|:-------------------------:|
    | [![The Things Network Catalunya](/imatges/ttn.png) *The Things Network Catalunya*](https://thethingsnetwork.cat/index.php/The_Things_Network_Catalunya) | [![FemProcomuns](/imatges/femprocomuns.png) *FemProcomuns*](http://www.femprocomuns.cat/) | [![Free Knowledge Institute](/imatges/fki.png) *Free Knowledge Institute*](https://pangea.org/)
    | [![Softcatalà](/imatges/softcatala.jpg) *Softcatalà*](https://softcatala.org) | [![Empoderamiento Digital Valladolid](/imatges/edvalladolid.png) *Empoderamiento Digital Valladolid*](https://empoderamientodigitalvalladolid.noblogs.org/)

    </div>

* Simpatitzants

   Els simpatitzants donen suport i difonen els continguts de La Guia. Envia'ns un correu a [info@apoderamentdigital.cat](mailto:info@apoderamentdigital.cat) si vols aparèixer com simpatitzant de La Guia.

    <div id="entitats" markdown="1">

    | | | |
    |:-------------------------:|:-------------------------:|:-------------------------:|
    | [![Xarxa d'Economia Solidària](http://xes.cat/wp-content/uploads/2017/05/logotip-xes.png) *Xarxa d'Economia Solidària*](http://xes.cat/) | [![CoopDevs](http://coopdevs.org/assets/img/logo.svg) *CoopDevs*](http://coopdevs.org) | [![Dimmons](https://www.uoc.edu/portal/_resources/common/imatges/in3/dimmons_proba.png) *Dimmons*](https://www.uoc.edu/portal/ca/in3/recerca/grups/digital_commons)
    | [![LliureTIC](https://lliuretic.cat/sites/default/files/logo-home.png) *LliureTIC*](https://lliuretic.cat/) | [![DonesTech](/imatges/donestech.svg) *DonesTech*](https://donestech.net/) | [![Fundación Goteo](/imatges/goteo.png) *Fundación Goteo*](https://goteo.org/) |

    </div>
