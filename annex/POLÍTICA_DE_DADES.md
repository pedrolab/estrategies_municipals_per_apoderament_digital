# Propostes references a Política de Dades
Llei 19/2014, de transparència, accés a la informació pública i bon govern aprovada pel Parlament de Catalunya obliga als municipis catalans a posar a disposició de la ciutadania les dades amb que gestionen les seves competències, exceptuant, és clar, aquelles protegides de caire personal o restringit.
Per bé que en aplicació d'aquesta llei, la gran majoria dels municipis catalans han afegit un apartat de dades obertes, en la majoria dels casos la informació que és mostra és nula o de poc valor.

Cal, primerament, des de l'àmbit local entendre que la obertura de dades públiques, no només significa un exercici de transparència, sino que permet l'edificació de valor real per part de la ciutadania (associacions, per exemple) o els negocis locals del municipi. Cal també, tal i com demana la llei, que des del municipi s'ofereixi aquesta informació de maneraa proactiva, sense que hagi de ser el ciutadà que la demani a l'administració corresponent.

En totes les administracions, però molt sovint en les petites locals, els treballadors públics senten recel en mostrar les dades que generen i gestionen en el seu dia a dia. L'obertura de dades cerca justament trencar amb aquesta dinàmica nefasta per a la nostra ciutadania i per a la pròpia administració local. La política de dades obertes permet a un ajuntament internament, acabar amb les sitges de poder inyternes que tant limiten la seva capacitat de gestió real.

Les dades obertes contribueixen a crear una ciutadania més implicada, més activa. També és beneficiós per a la seva participació i col·laboració.
## Open data per defecte

**Objectius**
* Fer més transparent l'adminstració, alhora més propera a la ciutadania.
* Facilitar l'accés i explotació dels conjunts de dades públics.
* Fer creixer les col·leccions de dades de lliure accés.

**Proposta programàtica**
1. Oferir, de manera proactiva des de l'ajuntament, el conjunt de dades dels diferents àmbits que l'ajuntament disposa.
1. Fer publicitat activa d'aquestes dades per tal que aquestes arribin a la ciutadania i incentivar-ne la seva reutilització.
1. Facilitar la seva accessibilitat des del web de l'ajuntament. Mitjançant enllaços en el cas dels ajuntaments petits o oferint serveis a partir de portals de dades obertes en el cas dels ajuntaments grans.
1. Publicar sota llicències lliures i formats reutilitzables tota la informació pública generada o gestionada per l'administració local (des dels pressupostos municipals fins als temps dels semàfors, p.e.)
1. Publicar les dades crues generades per a la realització d'estudis finançats amb diners públics, de manera que es puguin comprovar els estudis o fer-ne derivats.
1. Cal que cada conjunt de dades obert especifiqui la seva llicència per a clarificar als usuaris l'abast de la seva reutilització.

### a tenir en compte
* s'ha d'insitir en la qualitat de les dades i no tant en la quantitat. Amb temes de transparencia això és especialment important, les dades agregades també poden ser molt útils.
* s'ha de fer alguna restricció en l'us de les dades? S'ha de fer pagar alguna cosa al agunes empreses?
* facilitar la federació dels diferents datasets per poder fer cerques sobre tots alhora.
* l'ajuntament hauria de treballar directament amb les dades publicades.
* els serveis públics derivats d'una [concessió público-privada (PPP)][PPP] no
  poden estar exclosos de les polítiques de dades. Per a assegurar la
  disponibilitat i lliure accés als conjunts de dades derivats d'un PPP cal que
  l'administració especifiqui clarament els requisits en política de dades
  durant la licitació i el contracte del servei.

[PPP]: https://en.wikipedia.org/wiki/Public%E2%80%93private_partnership


  * [Ajuntament de Gavà](https://gavaobert.gavaciutat.cat/)

- Es van obrir moltes dades. Per exemple, padrons de l'IBI perquè els ciutadans sabessin abans de comprar una casa quan pagarien d'impost municipal, totes les factures de l'ajuntament 15 dies després d'haver estat registrades a comptabilitat, etc.
- L'ús que se n'ha fet és limitat ja que es tracta d'un ajuntament petit i perquè encara no hi ha gaire cultura d'aprofitament de les dades obertes per part de ciutadans i empreses.

  * [Ciutat de Nova York](https://opendata.cityofnewyork.us/)

- Portal molt ben fet i comprensiu.
- Diverses empreses utilitzen les dades per fer-ne productes que les necessiten. Per exemple, un portal immobiliari t'aconsella segons el teu perfil socio-democràfic on hauries de comprar o llogar apartament, gràcies a dades obertes.

  * [Regió de Helsinki](https://hri.fi/en_gb/)

- Ha georeferenciat i publicat en obert el mapa de barreres arquitectoniques de la ciutat.
- Creuant aquestes dades amb les de serveis, una empresa ha fet una applicació per a guiar gent cega.


## Privacitat de les dades de caràcter personal

**Objectius**
* Establir criteris ètics per a la explotació de les dades de caràcter personal.
* Garantir, la privacitat de les dades de caràcter personal, administrant-les de manera més conscient, reduint els riscos derivats de la seva explotació.

**Proposta programàtica**
1. Administrar les dades de caràcter personal únicament dins de l'administració i seguint estrictes criteris de seguretat, garantint els mitjans tècnics i els coneixements per gestionar-les sense dependre de tercers.
1. Limitar la recollida de dades de caràcter personal a aquells casos en què prèviament s'han establert les finalitats concretes per a les que seran utilitzades, minimitzant, així, la quantitat de dades personals que es recullen. En aquest sentit, si per exemple es recullen dades per entendre com es mouen les persones dins d'una ciutat i poder, d'aquesta manera, millorar el transport públic; aquestes dades no s'han d'utilitzar per estudiar els seus hàbits de consum.
1. Implementar mecanismes per donar un màxim, no el mínim legal GDPR, de serveis per saber i corregir o esborrar dades personals dels sistemes de l'administració.
1. No fer servir serveis webs privatius (enlloc de canals públics) per comunicació que comercien amb les dades de la gent, inclosos no fer servir trackers i serveis de publicitat de Google, Twitter, Facebook, etc. Fer servir en tot cas les xarxes socials de manera accessòria.
1. En principi nomes per transparencia presupuestaria pero sense compartir metadades es imposible aprofitar el potencial de les dades pq cada ajuntament pot etiqueta les seves dades ara per ara de forma diferent tot i que fan referencia a lo mateix i aixi es imposible interoperar. Les dades x tenir valor han de ser en molts casos big per tal caldria intruduir el compromis de crear un estandart de metadades municipal pq sense metadades comunes es imposible generar massa critica, x aixo obama va aprovar la Data Act a USA  https://www.grants.gov/learn-grants/grant-policies/data-act-2014.html

**Experiències inspiradores**
  * catsalut
  * comunitat europea
  * https://www.decode.com/
  * https://edri.org/press-release-gdpr-philosophy-respect/
  * https://gdprexplained.eu/
  * https://edri.org/a-guide-individuals-rights-under-gdpr/
