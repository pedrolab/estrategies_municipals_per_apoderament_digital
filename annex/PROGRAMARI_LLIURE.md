# Propostes sobre Programari Lliure

### Experiències inspiradores

  * Projecte Decidim.org: s'utilitza ja a molts municipis
  * TODO: UK libreoffice, Austràlia, Nova Zelanda.
  * Migració a programari lliure de l'Ajuntament de Saragossa
  * Munic: https://www.kdab.com/the-limux-desktop-and-the-city-of-munich/ (beneficis guanyats amb coses que ha fet aquesta consultora). TODO: mirar com presentar-ho, és un cas controvertit.
  * Hi ha multitud d'organitzacions fent servir Linux i programari lliure (llista no actualitzada): https://en.wikipedia.org/wiki/List_of_Linux_adopters
  * Joinup. Share and reuse Interoperability solutions for public administrations, businesses and citizens: https://joinup.ec.europa.eu/
  * Linkat

### Referències

  * Campanya Public Money, Public Code: https://publiccode.eu/ca/
  * Projecte GNU: https://www.gnu.org/philosophy/government-free-software.en.html
  * Barcelona: https://ajuntament.barcelona.cat/digital/ca/documentacio
  * Manifest sobre l'ús del programari lliure a l'administració pública de Softcatalà https://www.softcatala.org/wiki/Admpub/Manifest_sobre_l%27%C3%BAs_del_programari_lliure_a_l%27administraci%C3%B3_p%C3%BAblica
