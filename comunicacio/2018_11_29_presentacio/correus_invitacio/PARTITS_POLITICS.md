#### subject:
Invitació a la presentació de les Estratègies Municipals per a l'Apoderament Digital

#### body:
Bon dia,

Els últims mesos hem estat treballant, conjuntament amb una dotzena d'entitats (Sobtec, Barcelona Free Software, Pangea, Colectic, Pirates de Catalunya, Xarxa d'Innovació Pública, GufiNet/Exo, The Things Network Catalunya, Fem Procomuns, Free Knowledge Institute) en les [Estratègies Municipals per a l'Apoderament Digital](https://apoderamentdigital.cat), 44 mesures que farem arribar als partits polítics que concorreran a les eleccions municipals del 2019, per tal que les incorporin als seus programes electorals, mirant d'avançar, així, en la construcció de sobirania tecnològica, també des dels municipis. Com veureu, la guia recull aquestes mesures, però també objectius i línies d'actuació concretes, i estem treballant en un seguit d'eines i recursos per a facilitar la posada en pràctica de les diverses propostes.

El proper dijous 29 de novembre, a les 19h, a l'espai [Inceptum](https://inceptum.org/) farem un acte de presentació de la guia i les propostes, al qual us convidem a venir. A partir del mes de desembre, iniciarem formalment la ronda de contactes amb els partits polítics i farem un seguiment de les trobades, per veure on s'incorporen les propostes, i si s'acaben desenvolupant o no. Per a qualsevol cosa, podeu contactar-nos a info@apoderamentdigital.cat.
